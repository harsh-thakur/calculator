import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'calculator';
 // Updated Css and Refactored Code
  displayString = '';
  opernadOne: number;
  operandTwo: number;
  isOperatorSet = false;
  operator = '';
  constructor() {
  }

  ngOnInit() {
  }
  // 2+3*343
  append(key) {
    if ('/*+-'.indexOf(key) !== -1) {
      const lastKey = this.displayString[this.displayString.length - 1];
      if ('/*+-'.indexOf(lastKey) !== -1) {
        this.isOperatorSet = true;
      }
      if (this.isOperatorSet || this.displayString === '') { return; }
      this.opernadOne = +(this.displayString);
      this.operator = key;
      this.isOperatorSet = true;
    }
    this.displayString += key;
  }
  calculate() {
    // 7 + 2*3 - 9;
    // 7 + 9
    this.operandTwo = +(this.displayString.split(this.operator)[1]);
    switch (this.operator) {
      case '*': this.displayString = (this.opernadOne * this.operandTwo).toString();
                break;
      case '/': this.displayString = (this.opernadOne / this.operandTwo).toString();
                break;
      case '+': this.displayString = (this.opernadOne + this.operandTwo).toString();
                break;
      case '-': this.displayString = (this.opernadOne - this.operandTwo).toString();
                break;
      default:
        break;
    }
  }
}
